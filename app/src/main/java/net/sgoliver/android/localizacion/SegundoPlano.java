package net.sgoliver.android.localizacion;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by alumneDAM on 19/04/2018.
 */

public class SegundoPlano extends Service implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener{
    private LocationRequest locRequest;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    public  String matricula = "CHMJaa";
    private GoogleApiClient apiClient;
    public static final int RC_SIGN_IN = 1;

    private static final String LOGTAG = "android-localizacion";


    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseUser user;

    @Override

    public void onCreate() {

//Iniciatlitzem la base de dades i la autenticacio

        database = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
//Inicialitzem l'apiClient
        apiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

//En el AuthStateListener agafem el nom d'usuari per poder fer les actualitzacions al autobus pertinent
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                user = firebaseAuth.getCurrentUser();

                Toast.makeText(SegundoPlano.this, user.getDisplayName(), Toast.LENGTH_LONG).show();

                if (user != null) {
                    matricula = user.getDisplayName();

                    Toast.makeText(SegundoPlano.this, matricula, Toast.LENGTH_LONG).show();
                } else {

                }
            }

        };

        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }


//Aquest metode s'encarrega de activar les actualitzacions de localitzacions
    private void enableLocationUpdates() {

        locRequest = new LocationRequest();
        locRequest.setInterval(2000);
        locRequest.setFastestInterval(1000);
        locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        startLocationUpdates();

        LocationSettingsRequest locSettingsRequest =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locRequest)
                        .build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        apiClient, locSettingsRequest);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        Log.i(LOGTAG, "Configuración correcta");
                        startLocationUpdates();

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                            Log.i(LOGTAG, "Se requiere actuación del usuario");

                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(LOGTAG, "No se puede cumplir la configuración de ubicación necesaria");
                        break;
                }
            }
        });


    }

//Aquest metode s'encarrega de fer les actualitzacions, agafant la latitud i la longitud en cada moment
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(SegundoPlano.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //Ojo: estamos suponiendo que ya tenemos concedido el permiso.
            //Sería recomendable implementar la posible petición en caso de no tenerlo.

            Log.i(LOGTAG, "Inicio de recepción de ubicaciones");

            LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locRequest,SegundoPlano.this);
        }
    }

    //Metode que s'executa al iniciar el servei en segon pla
    @Override
    public int onStartCommand(Intent intenc, int flags, int idArranque) {
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                user = firebaseAuth.getCurrentUser();

                Toast.makeText(SegundoPlano.this, user.getDisplayName(), Toast.LENGTH_LONG).show();

                if (user != null) {
                    matricula = user.getDisplayName();

                    Toast.makeText(SegundoPlano.this, matricula, Toast.LENGTH_LONG).show();
                }else{

                }
            }


        };


        if (!apiClient.isConnected()) apiClient.connect();


        return START_STICKY;

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    //Metode que inicia el proces d'actualitzacions al conectarse correctament
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(LOGTAG, "Conectado");

        enableLocationUpdates();

    }


    //Metodes per saber quins errors tenim
    @Override
    public void onConnectionSuspended(int i) {
        Log.e(LOGTAG, "Se ha interrumpido la conexión con Google Play Services");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOGTAG, "Error grave al conectar con Google Play Services");
    }


    //Metode per actualitzar la localitzacio
    @Override
    public void onLocationChanged(Location location) {

        Log.i(LOGTAG, "Ubicación recibida!");
        saveLocation(location);

    }
//Metode per guardar una nova localitzacio
    public void saveLocation (Location loc)
    {
        myRef = database.getReference("root");
        Ubicacion last = new Ubicacion(loc.getLatitude(), loc.getLongitude(), loc.getTime());
        myRef.child(matricula).push().setValue(last);
    }




}
